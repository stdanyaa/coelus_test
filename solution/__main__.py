from .solution import solution

if __name__ == '__main__':
    print('input field size (m1, m2)')
    m1, m2 = map(int, input().split())

    print('input rectangle shaped poliominos number')
    h1 = int(input())
    r_poli = []
    for i in range(h1):
        print('input rectangle polio shape (s1, s2)')
        s1, s2 = map(int, input().split())
        print('input rectangle polio power (n_i)')
        n = int(input())
        r_poli.append([(s1, s2), n])
    
    print('input L shaped poliominos number')
    h2 = int(input())
    l_poli = []
    for i in range(h2):
        print('input L polio shape (q1, q2)')
        q1, q2 = map(int, input().split())
        print('input L polio power (n_i)')
        n = int(input())
        l_poli.append([(q1, q2), n])

    print(solution([(m1, m2), r_poli, l_poli]))
    
