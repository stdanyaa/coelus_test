import numpy as np
import math
from .poliomino import L_poli, R_poli


def recursive_part(table: np.ndarray, 
                   next_steps: np.ndarray, 
                   figures_arr: list, 
                   figures_used_idxs: list, figures_left_idxs: list) -> bool:
    
    if not len(figures_left_idxs):
        return True
    if not len(next_steps):
        return False

    # iterate over figures
    for arr_idx, fig_idx in enumerate(figures_left_idxs):
        fig = figures_arr[fig_idx]

        # iterate over possible figure placements
        for shift in next_steps:

            # iterate over different figure projections
            for state, borders in fig.generate_projections():

                curr_proj = state+shift
                proj_x, proj_y = curr_proj.T

                if (((0,0) <= curr_proj).all()
                    and (curr_proj < table.shape).all() and
                    not table[proj_x, proj_y].any()):
                    
                        table_copy = table.copy()
                        table_copy[proj_x, proj_y] = 1
                        
                        curr_border_proj = borders+shift
                        new_borders = np.vstack([curr_border_proj, next_steps])
                        new_borders = [x for x in new_borders 
                                       if (((0, 0) <= x).all() and (x < table_copy.shape).all()
                                           and not table_copy[tuple(x)])]
                        new_borders = np.unique(new_borders, axis=0)
                        
                        if recursive_part(table_copy, new_borders,
                                          figures_arr,
                                          figures_used_idxs + [fig_idx],
                                          figures_left_idxs[:arr_idx]+figures_left_idxs[1+arr_idx:]):
                                                return True
    else:
        return False

    

def solution(params: list[tuple, list, list]) -> bool:

    # parse parameters
    table_shape, r_poli, l_poli = params

    # init table and area for placing figures
    table = np.zeros(shape=table_shape)
    if table_shape[0] < table_shape[1]:
        initial_area = np.array([(0, i) for i in range(math.ceil(table_shape[0]//2))])
    else:
        initial_area = np.array([(i, 0) for i in range(math.ceil(table_shape[1]//2))])

    # init figures
    figures = []
    for shape, count in r_poli:
        for i in range(count):
            figures.append(R_poli(*shape))
    for shape, count in l_poli:
        for i in range(count):
            figures.append(L_poli(*shape))

    # solution    
    if sum([fig.square for fig in figures]) > table_shape[0]*table_shape[1]:
        return False
    return recursive_part(table,
                          initial_area,
                          figures,
                          figures_used_idxs=list(),
                          figures_left_idxs=list(range(len(figures))))
