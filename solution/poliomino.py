import numpy as np

__T = np.array([0, 1, -1, 0]).reshape(2, -1)

def _rotate(figure: np.ndarray):
    return np.dot(figure, __T)

def _mirror(figure: np.ndarray):
    return figure*(1,-1)

def _mirror_vs(figure: np.ndarray):
    return figure*(-1,1)


class R_poli:
    def __init__(self, s1: int, s2: int):
        self.w = s1
        self.h = s2
        self.square = s1*s2
        self.len = max(s1, s2)

        self.figure = np.array([(i, j) for i in range(self.h) for j in range(self.w)])
        self.border = np.array([(i, -1) for i in range(-1,self.w+1)]
                               + [(self.w, j) for j in range(-1,self.h+1)]
                               + [(i, self.h) for i in range(-1,self.w+1)]
                               + [(-1, j) for j in range(-1,self.h+1)])
        self.border = np.unique(self.border, axis=-1)
        self.figure_projections = [self.figure]
        self.border_projections = [self.border]
        for i in range(3): 
            self.figure_projections.append(_rotate(self.figure_projections[-1]))
            self.border_projections.append(_rotate(self.border_projections[-1]))
        
        self.figure_projections.append(_mirror(self.figure))
        self.border_projections.append(_mirror(self.border))
        for i in range(3):
            self.figure_projections.append(_rotate(self.figure_projections[-1]))
            self.border_projections.append(_rotate(self.border_projections[-1]))
        
    def generate_projections(self):
        for figure, border in zip(self.figure_projections, self.border_projections):
            yield figure, border
        

class L_poli:
    def __init__(self, q1: int, q2: int):
        self.q1 = q1
        self.q2 = q2
        self.square = q1+q2-1
        self.len = max(q1, q2)

        self.figure_projections = []
        self.border_projections = []

        # we can arange L shaped figure in two distinct ways from a single point
        for temp_q1, temp_q2 in [(q2, q1), (q1, q2)]:
            figure = np.concatenate([np.array([(0, i) for i in range(temp_q1)]),
                                     np.array([(j, temp_q1-1) for j in range(temp_q2)])], axis=0)
            # figure = np.unique(figure, axis=-1)
            border = np.array([(i, -1) for i in range(-1, 2)]
                               + [(1, j) for j in range(-1, temp_q1-1)]
                               + [(i, temp_q1-2) for i in range(1, temp_q2)]
                               + [(temp_q2, j+temp_q1-1) for j in range(-1,2)]
                               + [(i, temp_q1) for i in range(-1, temp_q2)]
                               + [(-1, j) for j in range(-1, temp_q1)])
            # border = np.unique(border, axis=-1)
            self.figure_projections.append(figure.copy())
            self.border_projections.append(border.copy())

            for i in range(3): 
                self.figure_projections.append(_rotate(self.figure_projections[-1]))
                self.border_projections.append(_rotate(self.border_projections[-1]))
            self.figure_projections.append(_mirror(figure))
            self.border_projections.append(_mirror(border))
            for i in range(3):
                self.figure_projections.append(_rotate(self.figure_projections[-1]))
                self.border_projections.append(_rotate(self.border_projections[-1]))


    def generate_projections(self):
        for figure, border in zip(self.figure_projections, self.border_projections):
            yield figure, border
