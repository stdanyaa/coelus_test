from solution.solution import solution

# example test
field = (3, 5)
r_poli = [((2,2), 1)]
l_poli = [((2,3), 1), ((2,2), 2)]
assert solution([field, r_poli, l_poli]) == True

# big table test
field = (16, 16)
r_poli = [((2,2), 64)]
l_poli = []
assert solution([field, r_poli, l_poli]) == True

# false test
field = (3, 3)
r_poli = []
l_poli = [[(2, 2), 3]]
solution([field, r_poli, l_poli]) == False

