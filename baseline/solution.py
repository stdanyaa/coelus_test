from .poliomino import solver

def solution(params: list[list, list, list]) -> bool:
    t_tiling, poliominos = solver.init_objects(params)
    result = solver.make_tiling(t_tiling, poliominos)
    return result

