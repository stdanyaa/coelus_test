from typing import List

from .poliomino import Poliomino, LPoliomino
from .tiling import TableTiling


def make_tiling(t_tiling: TableTiling, pols: List[Poliomino]):
    """
    Algorithm for finding a tiling of a rectangular area by a set of polyominoes.

    :param t_tiling: table tiling object with required width and height
    :param pols: list of poliominos
    :return: True if tiling is exists and False in other case
    """
    states = [t_tiling.put(pol) for pol in pols]
    k = 0
    while 0 <= k < len(pols):
        if next(states[k]):
            k += 1
        else:
            t_tiling.tables.pop()
            states[k] = t_tiling.put(pols[k])
            k -= 1
    if len(t_tiling.tables) - 1 == len(pols):
        return True
    else:
        return False


def init_objects(params: list[list, list, list]) -> tuple[TableTiling, list[Poliomino]]:
    field_shape, rectangle_poliominos, l_shaped_poliominos = params

    t_tiling = TableTiling(*field_shape)
    poliominos = []

    for (poli_parameters, poli_count) in rectangle_poliominos:
        for i in range(poli_count):
            poliominos.append(Poliomino(*poli_parameters))

    for (poli_parameters, poli_count) in l_shaped_poliominos:
        for i in range(poli_count):
            poliominos.append(LPoliomino(*poli_parameters))
    
    return t_tiling, poliominos
